# sisop-praktikum-modul-2-2023-AM-C09
### Anggota Kelompok C09
| Nama                         | NRP        |
| ---------------------------- | -----------|
| Farrela Ranku Mahhisa        | 5025211129 |
| Yohanes Teguh Ukur Ginting   | 5025211179 |
| Shazia Ingeyla Naveeda       | 5025211203 |


<h2>Daftar Isi</h2>

- [Soal 1](#soal-1) 
- [Soal 2](#soal-2) 
- [Soal 3](#soal-3) 
- [Soal 4](#soal-4)

## Soal 1

```c
int main (){
system("wget --output-document=binatang.zip 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq'");
```
Berfungsi untuk melakukan download dari link yang telah disediakan, dengan output binatang.zip

```c
pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
  if (child_id == 0) {
	char *argv[] = {"unzip", "binatang.zip", "-d", "list", NULL};
	execv("/bin/unzip", argv);
  } 
```
Berfungsi untuk membuat proses baru (child process), dimana proses yang dilakukan adalah mengunzip binatang.zip yang sudah didownload tadi ke direktori bernama 'list'

```c
else {
        	while ((wait(&status)) > 0);
		pindah();
        		char *folders[] = {"HewanAir", "HewanDarat", "HewanAmphibi", NULL};
			for (int i = 0; folders[i] != NULL; i++) {
			    pid_t zip_child_id = fork();
			    if (zip_child_id == 0) {
				char zip_file[50];
				sprintf(zip_file, "%s.zip", folders[i]);
				char *argv[] = {"zip", "-rq", zip_file, folders[i], NULL};
				execv("/usr/bin/zip", argv);
				exit(EXIT_SUCCESS);
				} 	else if (zip_child_id < 0) {
					exit(EXIT_FAILURE);
				    }
			}
	}
while ((wait(&status)) > 0);
	bersih();
	return 0;
}

```
Setelah child process selesai dijalankan, maka akan mengeksekusi fungsi pindah() yang akan menghasilkan folder hewan berdasarkan habitatnya, kemudian membuat proses baru lagi yang berfungsi melakukan perulangan untuk men-zip ketiga folder habitat dengan rekursif dan tanpa mengeluarkan output sama sekali ke terminal. Setelah semua perintah dilakukan, makan fungsi bersih() akan dieksekusi, berfungsi untuk menghapus semua foler dan file yang sudah tidak dibutuhkan.

*FUNGSI UTILITAS*

#1. PINDAH()

```c
int pindah(){
   int a = system(
       "bash -c '"
	"rand=$(ls list | shuf -n 1); "
	"rand=${rand%.*}; "
	"clear; "
	"echo \"GRAPE_KUN  HARI INI  AKAN MENJAGA : $rand\"; "
        "while true; do "
        "air_files=$(find ./list -maxdepth 1 -name \"*_air.jpg\" -print); "
        "amphibi_files=$(find ./list -maxdepth 1 -name \"*_amphibi.jpg\" -print); "
        "darat_files=$(find ./list -maxdepth 1 -name \"*_darat.jpg\" -print); "
        "if [[ -n $air_files ]]; then "
        "mkdir -p HewanAir && mv $air_files HewanAir/; "
        "fi; "
        "if [[ -n $amphibi_files ]]; then "
        "mkdir -p HewanAmphibi && mv $amphibi_files HewanAmphibi/; "
        "fi; "
        "if [[ -n $darat_files ]]; then "
        "mkdir -p HewanDarat && mv $darat_files HewanDarat/; "
        "fi; "
        "if [[ -z $air_files && -z $amphibi_files && -z $darat_files ]]; then "
	"break; "
        "fi; "
        "done'" );
    return a;
}

```

Fungsi pindah() memiliki 2 fungsi utama :
1. Membersihkan terminal dan melakukan print hewan yang harus dijaga grape-kun.
2. Memilah file dan memasukkan ke folder sesuai habitatnya

Fungsi ini menggunakan system dengan memanfaatkan shell script. Pertama, mencari satu file dalam folder list secara acak menggunakan shuffle lalu diassign ke variabel rand, dan karena masih ada format '.zip' nya, maka formatnya dipotong agar hanya mengandung nama hewan dan habitatnya saja kemudian menampilkan ke terminal.

Fungsi ini kemudian akan melakukan pemilahan file secara rekursif hingga folder list benar-benar kosong. Pertama fungsi akan membuat variabel air_files, amphibi_files, dan darat files, yang nantinya menyimpan nama setiap hewan berdasarkan habitatnya.

```c
air_files=$(find ./list -maxdepth 1 -name \"*_air.jpg\" -print)
```
Line di atas berguna untuk mencari file yang memiliki akhiran _air.jpg lalu diassign ke variabel air_files.

```c
 "if [[ -n $air_files ]]; then "
        "mkdir -p HewanAir && mv $air_files HewanAir/; "
        "fi; "
```
Bila ditemukan nama file hewan yang berhabitat di air, maka folder 'HewanAir' akan dibuat (bila belum ada), lalu file tadi akan dipindah ke folder baru tersebut.

Perintah di atas berlaku juga untuk HewanDarat dan HewanAmphibi, sampai folder list benar-benar kosong.

#2. BERSIH()

```c
int bersih(){
int x = system(
	"bash -c '"
        "rm binatang.zip && rmdir list && rm -r HewanAir HewanDarat HewanAmphibi; '" );
return x;
}
```
Fungsi di atas menggunakan system dan akan melakukan penghapusan ke seluruh folder dan file yang tidak disebutkan penggunaannya dalam soal, serta untuk menghemat penyimpanan. File binatang.zip harus dihapus karena seluruh isinya sudah diekstrak dan dipilah. Folder list, HewanAir, HewanDarat, HewanAmphibi juga harus dihapus menggunakan rm -r karena sudah tidak terpakai lagi.


## Soal 2

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

Catatan :

- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat - berjalan secara bersamaan (overlapping)\

### Soal A

Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp `[YYYY-MM-dd_HH:mm:ss]`

**Implementasi Code**

```c
int main(int argc, char *argv[]) {

    if (argc < 2) {
     printf("Usage: %s [-a | -b]\n", argv[0]);
     exit(EXIT_FAILURE);
    }
  pid_t pid, sid;// Variabel untuk menyimpan PID
  pid = fork();// Menyimpan PID dari Child Process
  if (pid < 0) exit(EXIT_FAILURE);
  if (pid > 0) exit(EXIT_SUCCESS);
  umask(0);
  sid = setsid();
  if (sid < 0) exit(EXIT_FAILURE);

  pembunuh(argv,pid,sid);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    // Tulis program kalian di sini
    pid_t pid;
    char foldname[30];
    struct tm *local_time;
    time_t current_time = time(NULL);
    local_time = localtime(&current_time);
    strftime(foldname, sizeof(foldname), "%Y-%m-%d_%H:%M:%S", local_time);
    buatfolder(foldname);

    pid = fork();
    if(pid<0)exit(EXIT_FAILURE);
    if(pid==0){
        donimg(foldname);
    }
    sleep(30);
  }
}
```

**Penjelasan :**

- Pada persoalan pertama diminta untuk membuat folder setiap 30 detik dengan penamaan `[YYYY-MM-dd_HH:mm:ss]` dan pada soal diminta untuk dalam bentuk daemon prosess, maka dibuat terlebih dahulu daemon prosesnnya

```c
pid_t pid, sid;// Variabel untuk menyimpan PID
  pid = fork();// Menyimpan PID dari Child Process
  if (pid < 0) exit(EXIT_FAILURE);
  if (pid > 0) exit(EXIT_SUCCESS);
  umask(0);
  sid = setsid();
  if (sid < 0) exit(EXIT_FAILURE);
```

- Pertama parent process membuat child process lalu parent prosess tersebut di kill lalu kita ubah permission file tersebut sehingga dapat diakses full dengan `umask (0)`. Lalu child yang masih jalan diberikan `sid` agar child tidak menjadi orphan process dengan `sid = setsid()`

```c++
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
```

- Lalu karena daemon process tidak boleh menggunakan terminal maka file descriptor standarnya akan dihapus (STDIN, STDOUT, STDERR)

```c
while(1){
    ...
}
```

- Disinilah perintah/proses yang ingin di jalankan secara daemon ditulis

```c
    pid_t pid;
    char foldname[30];
    struct tm *local_time;
    time_t current_time = time(NULL);
    local_time = localtime(&current_time);
    strftime(foldname, sizeof(foldname), "%Y-%m-%d_%H:%M:%S", local_time);
```

- Pertama deklarasikan foldname untuk menyimpan pembuatan nama folder, lalu deklarasikan struct tm yang dimana struct yang menyimpan waktu sesuai standar detik,jam,hari,bulan,tahun,menit. lalu pada `time_t current_time(NULL)` digunakan untuk mendapatkan waktu saat ini tetapi dalam bentuk detik Epoch Unix yang dihitung dari 1 Januari 1970. `local_time = localtime(&current_time)` digunakan untuk mengubah detik yang didapatkan menjadi struktur penamaan waktu pada umumnya. Lalu `strftime(foldname, sizeof(foldname), "%Y-%m-%d_%H:%M:%S", local_time); ` digunakan untuk mengubah waktu yang disimpan di `local_time` menjadi penamaan file yang diinginkan yaitu `[YYYY-MM-dd_HH:mm:ss]` dan dimasukkan ke dalam `foldname` dengan besar maksimal adalah `sizeof(foldname)`.

```c++
buatfolder(foldname);
```

- Lalu jalankan fungsi buatfolder untuk membuat folder dengan penamaan yang tentukan.

```c++
void buatfolder(char *nama){
    pid_t tid;
    int status;
    tid = fork();
    if(tid<0) exit(EXIT_FAILURE);
    if(tid==0) execlp("mkdir","mkdir",nama,NULL);
    else{
        waitpid(tid,&status,0);
    }
}
```

- Fungsi buatfolder bekerja dengan membuat child dengan `fork()` lalu pada child nya yaitu pada `if(tid==0)` akan membuat folder dengan `exec()` yaitu dengan `execlp("mkdir","mkdir",nama,NULL)` tetapi di dalam parent yaitu dalam else menunggu child selesai sebelum melanjutkan

```c++
    pid = fork();
    if(pid<0)exit(EXIT_FAILURE);
    if(pid==0){
        donimg(foldname);
    }
    sleep(30);
```

- Lalu di akan diberikan `sleep(30)` agar perintah di atasnya di jalankan setiap 30 detik sekali

### Soal B dan C

#### Soal B

- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran `(t%1000)+50` piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp `[YYYY-mm-dd_HH:mm:ss]`.

#### Soal c

- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

**Implementasi Code**

```c++
void donimg(char *folder){
    pid_t pid;
    int status;
    char nama[60],donl[50];
    // chdir(folder);
    for(int i=0;i<15;i++){
        pid = fork();
        if(pid < 0) exit(EXIT_FAILURE);
        if(pid==0){
            int pxl;
            struct tm *local_time;
            char lokasi[30];
            sprintf(lokasi, "%s",folder);
            time_t current_time = time(NULL);
            pxl = (current_time % 1000) + 50;
            local_time = localtime(&current_time);
            char waktu[30];
            strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time);
            // strftime(nama,sizeof(nama),"%s/%Y-%m-%d_%H:%M:%S.png",lokasi,local_time);
            sprintf(nama,"%s/%s",lokasi,waktu);
            sprintf(donl,"https://picsum.photos/%d",pxl);

            execlp("wget","wget","-q","-O",nama,donl,NULL);
        }else {
            waitpid(pid,&status,0);
            sleep(5);
        }
    }
    pid = fork();
    if(pid<0) exit(EXIT_FAILURE);
    if(pid==0){
        char namazip[50];
        sprintf(namazip,"%s.zip",folder);
        execlp("zip","zip","-r",namazip,folder,NULL);
    }else{
        waitpid(pid,&status,0);
        pid = fork();
        if (pid==0) execlp("rm","rm","-r",folder,NULL);
    }
    exit(EXIT_SUCCESS);

}

int main(){
    ...
    pid = fork();
    if(pid<0)exit(EXIT_FAILURE);
    if(pid==0){
        donimg(foldname);
    }
}
```

**Penjelasan :**

```C++
int main(){
    ...
    pid = fork();
    if(pid<0)exit(EXIT_FAILURE);
    if(pid==0){
        donimg(foldname);
    }
}
```

- Untuk menyelesaikan persoalan B dan C dilakukan bersamaan. Untuk mendownload gambar dari link yang sudah diberikan dilakukan di child process sehingga dibuat child proses dengan `fork()` lalu pada `if(pid==0)` akan memanggil fungsi `donimg(foldname)` dimana akan mendownload gambar didalam folder `foldname`. Didalam fungsi `donimg()` sendiri akan menyelesaikan persoalan B dan C.

```c++
void donimg(char *folder){
    pid_t pid;
    int status;
    char nama[60],donl[50];
    // chdir(folder);
    for(int i=0;i<15;i++){
        pid = fork();
        if(pid < 0) exit(EXIT_FAILURE);
        if(pid==0){
            int pxl;
            struct tm *local_time;
            char lokasi[30];
            sprintf(lokasi, "%s",folder);
            time_t current_time = time(NULL);
            pxl = (current_time % 1000) + 50;
            local_time = localtime(&current_time);
            char waktu[30];
            strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time);
            sprintf(nama,"%s/%s",lokasi,waktu);
            sprintf(donl,"https://picsum.photos/%d",pxl);
            execlp("wget","wget","-q","-O",nama,donl,NULL);
        }else {
            waitpid(pid,&status,0);
            sleep(5);
        }
    }
    pid = fork();
    if(pid<0) exit(EXIT_FAILURE);
    if(pid==0){
        char namazip[50];
        sprintf(namazip,"%s.zip",folder);
        execlp("zip","zip","-r",namazip,folder,NULL);
    }else{
        waitpid(pid,&status,0);
        pid = fork();
        if (pid==0) execlp("rm","rm","-r",folder,NULL);
    }
    exit(EXIT_SUCCESS);

}
```

- Pada fungsi `donimg()`akan dilakukan perulangan 15 kali karena akan mendownload gambar sebanyak 15 kali

```c++
        pid = fork();
        if(pid < 0) exit(EXIT_FAILURE);
        if(pid==0){
            int pxl;
            struct tm *local_time;
            char lokasi[30];
            sprintf(lokasi, "%s",folder);
            time_t current_time = time(NULL);
            pxl = (current_time % 1000) + 50;
            local_time = localtime(&current_time);
            char waktu[30];
            strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time);
            sprintf(nama,"%s/%s",lokasi,waktu);
            sprintf(donl,"https://picsum.photos/%d",pxl);
            execlp("wget","wget","-q","-O",nama,donl,NULL);
        }else {
            waitpid(pid,&status,0);
            sleep(5);
        }
```

- Pertama akan dibuat child terlebih dahulu dengan `fork()` lalu pada child tersebut pertama mendapatkan waktu sekarang dengan `time_t current_time = time(NULL)` berbentuk `detik Epoch Unix`. `sprintf(lokasi, "%s",folder)` pada perintah ini akan menyimpan variabel folder yang beruka string ke dalam char of array yang bernama lokasi. Lalu untuk mendapatkan ukuran pixel gambar yang akan didownload sesuai ketentuan soal maka dilakukan ` pxl = (current_time % 1000) + 50`

```c++
local_time = localtime(&current_time);
char waktu[30];
strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time);
sprintf(nama,"%s/%s",lokasi,waktu);
```

- Untuk menentukan penamaan file akan dilakukan ` strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time)` seperti sebelumnya lalu `sprintf(nama,"%s/%s",lokasi,waktu)` untuk menentukan penamaan file nya beserta foldernya karena pada proses fungsi `donimg()` daemon berjalan di directory awal bukan dalam directory folder yang dibuat sehinnga pada penamaan file nanti saat di download akan ditulis path foldernya juga.

```c++
sprintf(donl,"https://picsum.photos/%d",pxl);
execlp("wget","wget","-q","-O",nama,donl,NULL);
```

- Lalu ukuran pixel yang akan didownload di gabungkan dengan link downloadnya lalu disimpan di variabel donl
- Lalu akan dilakukan download gambar dari link dengan fungsi `execlp("wget","wget","-q","-O",nama,donl,NULL)`. Dimana akan menggunakan command wget dengan argument `-q` agar berjalan secara silent, tidak mengeluarkan output di terminal dan argument `-O` agar file dapat didownload dengan penamaan file buatan sendiri yaitu yang disimpan pada variabel `nama` dan dari link `donl`.

```c++
else {
    waitpid(pid,&status,0);
    sleep(5);
}
```

- Lalu pada parent, akan menunggu child selesai terlebih dahulu dengan `waitpid(pid,&status,0)` lalu akan menunggu selama 5 detik karena permintaan soal adalah mendownload gambar setiap 5 detik.

```c++
pid = fork();
    if(pid<0) exit(EXIT_FAILURE);
    if(pid==0){
        char namazip[50];
        sprintf(namazip,"%s.zip",folder);
        execlp("zip","zip","-r",namazip,folder,NULL);
    }else{
        waitpid(pid,&status,0);
        pid = fork();
        if (pid==0) execlp("rm","rm","-r",folder,NULL);
    }
    exit(EXIT_SUCCESS);
```

- Lalu pada soal C diminta untuk melakukan zip dan menghapus folder setelah mendownload 15 gambar
- Diawal dilakukan pembuatan child lagi lalu pada parent akan melakukan `waitpid(pid,&status,0)`agar child berjalan terlebih dahulu.

```c++
if(pid==0){
    char namazip[50];
    sprintf(namazip,"%s.zip",folder);
    execlp("zip","zip","-r",namazip,folderNULL);
}
```

- Pada child akan melakukan zip folder terlebih dahulu dengan menyimpan terlebih dahulu nama `folder.zip` kedalam variabel `namazip` lalu dilakukan zip dengan `execlp("zip","zip","-r",namazip,folderNULL) ` dimana menggunakan argumen `-r` dimana akan melakukukan rekursi kedalam directory yang akan dizip sehingga semua file didalam folder akan di zip

```c++
else{
    waitpid(pid,&status,0);
    pid = fork();
    if (pid==0) execlp("rm","rm","-r",folder,NULL);
}
    exit(EXIT_SUCCESS);
```

- Lalu pada parent setelah melakukan wait, akan melakukan penghapusan folder yang sudah di zip dengan menggunakan `execlp("rm","rm","-r",folder,NULL)` dimana menggunakan argument `-r` dimana akan melakukan rekursif kedalam folder yang akan dihapus dimana semua file didalam folder tersebut akan dihapus

### Soal D dan E

#### Soal D

- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

#### Soal E

- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete)

**Implementasi Code :**

```c++
void pembunuh(char *argv[],pid_t pid,pid_t sid){
    FILE *kiler = fopen("killer.c", "w");
    int status;
    char isi[1200];
    if(strcmp(argv[1],"-a")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n\n"
                "#include <sys/wait.h>\n"

                "int main() {\n"
                "    int status;\n"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"pkill\",\"pkill\",\"-9\",\"-s\",\"%d\",NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"All processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",sid);
    }
    if(strcmp(argv[1],"-b")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n"
                "#include <sys/wait.h>\n"

                "int main() {\n"
                "    int status;"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"kill\", \"kill\", \"-9\", \"%d\", NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"Processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",getpid());
    }
    fclose(kiler);

    pid = fork();
    if(pid==0){
        execlp("gcc","gcc","killer.c","-o","killer",NULL);
    }else waitpid(pid,&status,0);

    pid = fork();
    if(pid==0){
        execlp("rm","rm","killer.c",NULL);
    }else waitpid(pid,&status,0);

}

int main(){
    ...
    pembunuh(argv,pid,sid);
    ...
}
```

- Untuk menyelesaikan persoal D dan E dilakukan bersamaan.

```c++
FILE *kiler = fopen("killer.c", "w");
    int status;
    char isi[1200];
```

- Pada fungsi `void pembunuh(char *argv[],pid_t pid,pid_t sid)` di awal akan membuat file dan di buka dengan `FILE *kiler = fopen("killer.c", "w")` dengan menggunakan argumen`w` file tersebut dibukan dengan ketentuan write sehingga dapat di edit

```c++
if(strcmp(argv[1],"-a")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n\n"
                "#include <sys/wait.h>\n"

                "int main() {\n"
                "    int status;\n"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"pkill\",\"pkill\",\"-9\",\"-s\",\"%d\",NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"All processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",sid);
    }
    if(strcmp(argv[1],"-b")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n"
                "#include <sys/wait.h>\n"

                "int main() {\n"
                "    int status;"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"kill\", \"kill\", \"-9\", \"%d\", NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"Processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",getpid());
    }
```

- Lalu disetiap if akan dilakukan pengecekan dengan `strcmp` untuk membandingkan argumen input saat menjalan file yaitu `-a || -b` dimana saat argumen `-a` akan menjalan perintah mode a yang secara garis besar akan membuat child proses dengan menjalan perintah `execlp("pkill","pkill","-9","-s","%d",NULL)` dimana `%d` merupakan sid daemon yang dibuat. Lalu setelah child selesai akan menghapus diri sendiri atau file yang dibuat dengan nama killer dengan `execlp("rm","rm","killer",NULL)`.

- Di mode b secara garis besar sama tetapi perintah kill yang dibuat adalah `execlp("kill", "kill", "-9", "%d", NULL)` dimana `%d` adalah pidnya. Lalu setelah itu akan mengapus diri sendiri dengan `execlp("rm","rm","killer",NULL)`.

```c++
 fclose(kiler);

    pid = fork();
    if(pid==0){
        execlp("gcc","gcc","killer.c","-o","killer",NULL);
    }else waitpid(pid,&status,0);

    pid = fork();
    if(pid==0){
        execlp("rm","rm","killer.c",NULL);
    }else waitpid(pid,&status,0);
```

- Setelah itu file yang diopen akan diclose dengan `fclose(kiler)` setelah itu akan dibuat child proses baru dan didalam child proses akan dilakukan compile file killer yang sudah ditulis sebelumnya dengan `execlp("gcc","gcc","killer.c","-o","killer",NULL)` setelah itu parent menunggu child terlebih dahulu lalu membuat child baru lagi untuk menghapus file.c yang dibuat dengan `execlp("rm","rm","killer.c",NULL)` tetapi parent menunggu child untuk `rm` terlebih dahulu sebelum akhirnya melanjutkan ke perintah berikutnya.

- Pemanggilan fungsi tersebut ditulis didalam fungsi main yaitu saat

```c++
  sid = setsid();
  if (sid < 0) exit(EXIT_FAILURE);

  pembunuh(argv,pid,sid);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
```

- saat pembuatan daemon proses, akan memanggil fungsi `pembunuh(argv,pid,sid)` dimaan akan melakukan passing argv,pid,sid kedalam fungsinya.

```c++
if (argc < 2) {
     printf("Usage: %s [-a | -b]\n", argv[0]);
     exit(EXIT_FAILURE);
    }
```

- Lalu diawal setelah int main akan dibuat error handling jika input tidak sesuai (tidak memberikan argumen `-a || -b`).


## Soal 3
Membuat 1 program C untuk membantun Ten Hag mengenal para pemain Manchester United. 

Berikut merupakan fungsi utama yang akan memanggil fungsi-fungsi untuk mengunduh, unzipping, penghapusan file, melakukan sortir, pengkelompokan dan pembuatan kesebelasan terbaik.

*IMPLEMENTASI*
```sh
int main(int argc, char *argv[]) {

    if (argc < 3) {
        printf("Usage: %s [bek] [gelandang] [penyerang]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    donlot(); sleep(10);
    uzip(); sleep(10);
    hapus(); sleep(10); 

    sortir(); sleep(10);

    tigac(); sleep(10);

    int bek,gelandang,penyerang;
    BuatTim(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
}
```
*PENJELASAN*

 Variable ``argc`` akan menyimpan jumlah argumen yang diberikan dan ``argv[0]`` akan menyimpan argumen tersebut dalam bentuk array. Dimana program akan menampilkan ``"Usage: %s [bek] [gelandang] [penyerang]"`` jika input yang diterima tidak sesuai yaitu argumen yang kurang dari 3 dan keluar dari program menggunakan fungsi ``exit()`` dengan status ``EXIT_FAILURE``.

 Jika jumlah argumen cukup maka program akan memanggil fungsi ``donlot()`` ``uzip()`` ``hapus()`` ``sortir()`` ``tigac()`` untuk melakukan pengurutan pemain dan setelah pengolahan data selesai, program akan memanggil fungsi ``BuatTim`` sesuai dengan input yang sudah diberikan untuk membentuk tim sekpak bola dengan kesebelasan terbaik.

#### 3. a. Program C akan mengunduh database para pemain. Lalu program C akan melakukan extract “players.zip” dan hapus file zip tersebut.
*IMPLEMENTASI*
```sh
void donlot(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("wget","wget","https://drive.google.com/u/0/uc?id=1uQtvHtZEMJwRIjrF1u_8MlpctucvxurT&export=download","-O","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void uzip(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("unzip","unzip","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void hapus(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("rm","rm","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}
```

*PENJELASAN*
```sh
pid=fork();
```
Akan membuat proses baru (child process) yang memiliki PID yang berbeda dengan proses utama. 

```sh
if(pid<0){
        exit(EXIT_FAILURE);
}
```
Jika nilai PID kurang dari 0 maka proses ``fork`` tidak akan dijalankan.

```sh
else if(pid==0){
        execlp();
}
```
Dan akan menjelankan proses ``fork`` jika PID bernilai sama dengan 0.

- Fungsi ``donlot()`` akan mengunduh file https://drive.google.com/u/0/uc?id=1uQtvHtZEMJwRIjrF1u_8MlpctucvxurT&export=download menggunakan perintah ``wget``.

- Fungsi ``uzip()`` akan melakukan unzip file yang sudah diunduh dengan perintah ``unzip``.

- Fungsi ``hapus()`` akan menghapus file zip dengan perintah ``rm``.

```sh
else{
        waitpid(pid, &status,0);
}
```
Parent process tidak menjalankan apapun namun akan menunggu child process hingga selesai dengan perintah ``waitpid``.

#### 3. b. Program C akan menghapus pemain yang bukan dari Manchester United yang ada di directory.

*IMPLEMENTASI*
```sh
void sortir(){
  DIR *dir;
  struct dirent *ent;
  char *dir_path = "./players";
  dir = opendir("players");
  if(dir != NULL){
    while((ent = readdir(dir))!=NULL){
      if(ent->d_type==DT_REG){
        if(strstr(ent->d_name, "ManUtd")==NULL){
          char *path = malloc(strlen(dir_path) + strlen(ent->d_name) + 2);
          sprintf(path, "players/%s", ent->d_name);
          remove(path)==0;
          free(path);
        }
      }
    }
  }
}
```
*PENJELASAN*

Fungsi ``sortir()`` akan membuka direktori players dengan fungsi ``opendir()`` dan menyimpan direktori dengan variable ``dir``.

fungsi akan melakukan looping jika direktori tidak kosong menggunakan fungsi ``readdir()``. Iterasi ini akan dijalankan sampai tidak ada algi file yang dapt dibaca. Menggunakan ``d_type`` setiap file yang dibaca akan diperiksa tipe filenya dan hanya file dengan tipe file reguler yang akan di proses.

File yang sudah memenuhi persyaratan akan disortir kembali apakah file tersebut memiliki string ManUtd menggunakan fungsi ``strstr()``. Jika string tersebut tidak ditemukan dalam file yang terpilih, maka file tersebut akan dihapus.

```sh
char *path = malloc(strlen(dir_path) + strlen(ent->d_name) + 2);
sprintf(path, "players/%s", ent->d_name);
```
Akan dilakukan alokasi memori dengan fungsi ``malloc()`` untuk variabel ``path`` yang digunakan untuk menyimpan string path file yang akan dihapus. Setelah berhasil dialokasikan memorinya, menggunakan fungsi ``sprintf()`` akan digabungkan string direktori "players" dan nama file menjadi satu string lengkap yang disimpan di variabel path.

String path file yang valid tersebut akan digunakan untuk menghapus file pada direktori players menggunakan fungsi ``remove()`` dan setelah berhasil dihapus, memori yang telah dialokasikan dibebaskan menggunakan fungsi ``free()``.

#### 3. c. Program C mengkategorikan pemain sesuai posisi (Kiper, Bek, Gelandang, Penyerang).

*IMPLEMENTASI*
```sh
void move(char *posisi){
    pid_t ppit;
    int status;

    DIR *dir;
    struct dirent *ent;
    char *dir_path = "players";

    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("Failed to open directory");
    }

    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            char *filename = ent->d_name;
            char *pos = strstr(filename, posisi);

            if (pos != NULL) { 
                char *path = malloc(strlen(dir_path) + strlen(filename) + 200); 
                char *dest_path = malloc(strlen(posisi) + strlen(dir_path) + 200);
                sprintf(path, "%s/%s", dir_path, filename); 
                sprintf(dest_path, "%s/%s",dir_path ,posisi);
                ppit = fork();
                if(ppit<0) exit(EXIT_FAILURE);
                else if(ppit==0) execlp("mv","mv",path,dest_path,NULL);
                else waitpid(ppit, &status,0);

                free(path);
                free(dest_path);
            }
        }
    }
    closedir(dir);
}
```
Fungsi ``move()`` akan membuka direktori players dengan fungsi ``opendir()`` dan menyimpan direktori dengan variable ``dir``. Jika direktori kosong maka program akan menampilkan ``"Failed to open directory"``

fungsi akan melakukan looping jika direktori tidak kosong menggunakan fungsi ``readdir()``. Iterasi ini akan dijalankan sampai tidak ada algi file yang dapt dibaca. Menggunakan ``d_type`` setiap file yang dibaca akan diperiksa tipe filenya dan hanya file dengan tipe file reguler yang akan di proses. Kode akan mencari apakah nama file tersebut mengandung nama posisi yang diberikan (misalnya, "Kiper") menggunakan ``strstr(filename, posisi)``.

Jika posisi tidak sama dengan NULL maka program akan menghitung alokasi memori path asal dan path tujuan dengan fungsi ``malloc()`` Setelah berhasil dialokasikan memorinya, menggunakan fungsi ``sprintf()`` akan:
- Menggabungkan direktori "players" dengan nama file menggunakan sprintf untuk membuat path lengkap file.
- Menggabungkan direktori "players" dengan posisi menggunakan sprintf untuk membuat direktori tujuan file.

File akan dipindahkan dari path asal ke path tujuan menggunakan perintah ``execlp("mv", "mv", path, dest_path, NULL)`` yang berada di dalam child process dan membebaskan memori yang telah dialokasikan dengan fungsi ``free()``




```sh
void tigac(){
    pid_t pid1,pid2,pid3,pid4;

    pid1=fork();
    if(pid1<0){
        exit(EXIT_FAILURE);
    }else if(pid1==0){
        pid_t cpid1=fork();
        if(cpid1<0) exit(EXIT_FAILURE);
        else if(cpid1==0) execlp("mkdir","mkdir","players/Kiper",NULL);
        else {
            waitpid(cpid1, &status,0);
            move("Kiper");
        }
        exit(0);
    }

    pid2=fork();
    if(pid2<0){
        exit(EXIT_FAILURE);
    }else if(pid2==0){
        pid_t cpid2=fork();
        if(cpid2<0) exit(EXIT_FAILURE);
        else if(cpid2==0) execlp("mkdir","mkdir","players/Bek",NULL);
        else {
            waitpid(cpid2, &status,0);
            move("Bek");
        }
        exit(0);
    }

    pid3=fork();
    if(pid3<0){
        exit(EXIT_FAILURE);
    }else if(pid3==0){
        pid_t cpid3=fork();
        if(cpid3<0) exit(EXIT_FAILURE);
        else if(cpid3==0) execlp("mkdir","mkdir","players/Gelandang",NULL);
        else {
            waitpid(cpid3, &status,0);
            move("Gelandang");
        }
        exit(0);
    }

    pid4=fork();
    if(pid4<0){
        exit(EXIT_FAILURE);
    }else if(pid4==0){
        pid_t cpid4=fork();
        if(cpid4<0) exit(EXIT_FAILURE);
        else if(cpid4==0) execlp("mkdir","mkdir","players/Penyerang",NULL);
        else {
            waitpid(cpid4, &status,0);
            move("Penyerang");
        }
        exit(0);
    }
    waitpid(pid1, &status,0);
    waitpid(pid2, &status,0);
    waitpid(pid3, &status,0);
    waitpid(pid4, &status,0);
}
```
Fungsi ``tigac()`` akan membuat 4 folder secara bersamaan untuk menyimpan pemain sesuai dengan posisi-posisinya ya itu bek, kiper, gelandang dan penyerang.
```sh
pid1=fork();
```
Akan membuat proses baru (child process) yang memiliki PID yang berbeda dengan proses utama. Berlaku juga untuk pembuatan ``pid2`` ``pid3`` ``pid4``. 

```sh
if(pid<0){
        exit(EXIT_FAILURE);
}
```
Jika nilai PID kurang dari 0 maka proses ``fork`` tidak akan dijalankan.

```sh
else if(pid==0){
        execlp();
}
```
Dan akan menjelankan proses ``fork`` jika PID bernilai sama dengan 0 dimana child process tersebut akan membuat child process lainnya yang akan membuat folder posisi dengan perintah ``execlp("mkdir", "mkdir", "players/[Posisi]", NULL)``.

```sh
else {
        waitpid(cpid1, &status,0);
        move("Posisi");
}
```
Parent process akan menunggu dengan perintah ``waitpid()`` dan setelah child process selesai, File akan dipindahkan sesuai dengan posisinya dengan perintah ``move("Posisi")``



#### 3. d. Program C membuat tim sepakbola dengan rating pemain tertinggi di setiap posisinya.
*IMPLEMENTASI*
```sh
typedef struct {
  int rating;
  char filename[300];
} Pemain;

int bandingkan_rate(const void* a, const void* b) {
  const Pemain* pemain_a = (const Pemain*)a;
  const Pemain* pemain_b = (const Pemain*)b;
  return pemain_b->rating - pemain_a->rating;
}

void ambil_rate_player(const char* position, Pemain* pemain, int* count) {
  struct dirent* entry; DIR* dir;
  char directory[100];

  sprintf(directory,"players/%s", position);
  dir = opendir(directory);

	if (dir) {
	    for (entry = readdir(dir); entry; entry = readdir(dir)) {
		if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG) {
		    int rating;
		    sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
		    strcpy(pemain[*count].filename, entry->d_name);
		    pemain[*count].rating = rating;
		    (*count)++;
		}
	    }
	    closedir(dir);
	} else {
	    exit(EXIT_FAILURE);
	}
}

void BuatTim(int bek, int gelandang, int penyerang) {
  Pemain  pemain_gelandang[100], pemain_penyerang[100], kiper[1], pemain_bek[100];
  int  jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;
  ambil_rate_player("Gelandang", pemain_gelandang, &jumlah_gelandang);
  ambil_rate_player("Penyerang", pemain_penyerang, &jumlah_penyerang);
  ambil_rate_player("Kiper", kiper, &jumlah_kiper);
  ambil_rate_player("Bek", pemain_bek, &jumlah_bek);

  qsort(pemain_gelandang, jumlah_gelandang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_penyerang, jumlah_penyerang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_bek, jumlah_bek, sizeof(Pemain), bandingkan_rate);

  char filepath[300];
  sprintf(filepath,"Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE* file = fopen(filepath, "w");

  if (file) {
  	int i;
	    fprintf(file, "Posisi Kiper:\n%s\n\n", kiper[0].filename);
	    
	    fprintf(file, "Posisi Bek:\n");
	    for (i = 0; i < bek && i < jumlah_bek; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_bek[i].filename);
	    }
	    
	    fprintf(file, "\n Posisi Gelandang:\n");
	    for (i = 0; i < gelandang && i < jumlah_gelandang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_gelandang[i].filename);
	    }
	    
	    fprintf(file, "\nPosisi Penyerang:\n");
	    for (i = 0; i < penyerang && i < jumlah_penyerang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_penyerang[i].filename);
	    }

	    fclose(file);
	    printf("Lineup saved\n");
  } else {
	    printf("Error\n");
	    exit(EXIT_FAILURE);
  }
}
```
*PENJELASAN*

Akan dibuat struck untuk pemain yaitu rating (integer) dan filename (array karakter berukuran 300).

Fungsi ``BuatTim()`` akan membuat tim dengan jumlah bek, gelandang dan penyerang yang telah diberikan dan memanggil fungsi ``ambil_rate_player()`` untuk mengumpulkan data pemain dalam setiap posisi, kemudian mengurutkan mereka berdasarkan rating menggunakan fungsi ``qsort`` dan      ``bandingkan_rate``.

Kemudian akan dibuat file text yang berisikan formasi tim dengan mencetak nama file pemain untuk setiap posisi sesuai jumlah yang diberikan. Jika file berhasil dibuat, program akan mencetak "Lineup saved", jika tidak, akan mencetak "Error" dan keluar dengan ``EXIT_FAILURE``.

## Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.
- Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

**Penjelasan :**
```c++
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Error: Invalid number of arguments\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Parse the arguments
    char* hour = argv[1];
    char* minute = argv[2];
    char* second = argv[3];
    char* script_path = argv[4];

    // Check if the arguments are valid
    if ((strcmp(hour, "*") != 0 && (atoi(hour) < 0 || atoi(hour) > 23))
        || (strcmp(minute, "*") != 0 && (atoi(minute) < 0 || atoi(minute) > 59))
        || (strcmp(second, "*") != 0 && (atoi(second) < 0 || atoi(second) > 59))) {
        printf("Error: Invalid time format\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Fork the process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork failed\n");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // Parent process
        printf("Cron job started with PID %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    // Child process

    // Create a new session
    if (setsid() < 0) {
        printf("Error: setsid() failed\n");
        exit(EXIT_FAILURE);
    }

    // Set the working directory to root
    if (chdir("/") < 0) {
        printf("Error: chdir() failed\n");
        exit(EXIT_FAILURE);
    }

    // Redirect standard output to the terminal
    int fd = open("/dev/tty", O_WRONLY);
    dup2(fd, STDOUT_FILENO);
    close(fd);

    // Run the cron job
    time_t current_time;
    struct tm *local_time;

    while (1) {
        // Get the current time
        current_time = time(NULL);
        local_time = localtime(&current_time);

        // Check if the time matches the cron schedule
        if ((strcmp(hour, "*") == 0 || local_time->tm_hour == atoi(hour))
            && (strcmp(minute, "*") == 0 || local_time->tm_min == atoi(minute))
            && (strcmp(second, "*") == 0 || local_time->tm_sec == atoi(second))) {
            // Run the script
            int ret = system(script_path);
            if (ret < 0) {
                printf("Error: Failed to execute script\n");
                exit(EXIT_FAILURE);
            }
        }

        // Sleep for 1 second
        sleep(1);
    }
    return 0;
}
```
**Penjelasan :**
```c++
if (argc != 5) {
        printf("Error: Invalid number of arguments\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
```
- Pada awal program akan dibuat error handling apabila input tidak sama dengan 5

```c++
// Parse the arguments
    char* hour = argv[1];
    char* minute = argv[2];
    char* second = argv[3];
    char* script_path = argv[4];
```
- Lalu variabel dari setiap argumen akan disimpen kedalam argumen setiap ketentuan yaitu hour, minute, second, script_path

```c++
// Check if the arguments are valid
    if ((strcmp(hour, "*") != 0 && (atoi(hour) < 0 || atoi(hour) > 23))
        || (strcmp(minute, "*") != 0 && (atoi(minute) < 0 || atoi(minute) > 59))
        || (strcmp(second, "*") != 0 && (atoi(second) < 0 || atoi(second) > 59))) {
        printf("Error: Invalid time format\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
```
- Lalu dibuatkan error handling lagi agar sesuai dengan ketentuan yaitu jam tidak lebih dari 23, menit dan detik tidak lebih dari 59 dan tidak boleh minus.

```c++
/ Fork the process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork failed\n");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // Parent process
        printf("Cron job started with PID %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    // Child process

    // Create a new session
    if (setsid() < 0) {
        printf("Error: setsid() failed\n");
        exit(EXIT_FAILURE);
    }

    // Set the working directory to root
    if (chdir("/") < 0) {
        printf("Error: chdir() failed\n");
        exit(EXIT_FAILURE);
    }

    // Redirect standard output to the terminal
    int fd = open("/dev/tty", O_WRONLY);
    dup2(fd, STDOUT_FILENO);
    close(fd);

```
- Lalu dibuatkan deaemon proces sesuai dengan template

```c++
// Run the cron job
    time_t current_time;
    struct tm *local_time;

    while (1) {
        // Get the current time
        current_time = time(NULL);
        local_time = localtime(&current_time);

        // Check if the time matches the cron schedule
        if ((strcmp(hour, "*") == 0 || local_time->tm_hour == atoi(hour))
            && (strcmp(minute, "*") == 0 || local_time->tm_min == atoi(minute))
            && (strcmp(second, "*") == 0 || local_time->tm_sec == atoi(second))) {
            // Run the script
            int ret = system(script_path);
            if (ret < 0) {
                printf("Error: Failed to execute script\n");
                exit(EXIT_FAILURE);
            }
        }

        // Sleep for 1 second
        sleep(1);
    }
```
- Lalu di dalam while akan dituliskan kode yang akan di jalankan. Karena pada persoalan ini secara garis besar disuruh membuat cronjob sendiri sehingga di dalam while akan melakukan run ke script yang diinginkan

```c++
// Get the current time
        current_time = time(NULL);
        local_time = localtime(&current_time);
```
- Pertama dapatkan waktu saat ini dan simpan dalam pemformatan waktu pada umumnya yaitu pada `struct tm *local_time`

```c++
// Check if the time matches the cron schedule
        if ((strcmp(hour, "*") == 0 || local_time->tm_hour == atoi(hour))
            && (strcmp(minute, "*") == 0 || local_time->tm_min == atoi(minute))
            && (strcmp(second, "*") == 0 || local_time->tm_sec == atoi(second))) {
            // Run the script
            int ret = system(script_path);
            if (ret < 0) {
                printf("Error: Failed to execute script\n");
                exit(EXIT_FAILURE);
            }
        }
```
- Dilakukan pengecekan waktu saat ini dengan waktu yang diinputkan saat menjalan kan program. Apabila sama dengan yang inputkan maka akan menjalankan script yang sudah dimasukkan atau path script yang sudah di berikan. Jika tidak bisa maka akan mengeluarkan pesan error

```c++
// Sleep for 1 second
    sleep(1);
```
- Lalu akan diberikan sleep 1 detik agar mengecek kesamaan waktu dengan saat ini apakah sama atau tidak disetiap detik berjalan
