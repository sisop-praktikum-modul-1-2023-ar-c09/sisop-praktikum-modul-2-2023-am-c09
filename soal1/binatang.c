#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int pindah(){
   int a = system(
       "bash -c '"
	"rand=$(ls list | shuf -n 1); "
	"rand=${rand%.*}; "
	"clear; "
	"echo \"GRAPE_KUN  HARI INI  AKAN MENJAGA : $rand\"; "
        "while true; do "
        "air_files=$(find ./list -maxdepth 1 -name \"*_air.jpg\" -print); "
        "amphibi_files=$(find ./list -maxdepth 1 -name \"*_amphibi.jpg\" -print); "
        "darat_files=$(find ./list -maxdepth 1 -name \"*_darat.jpg\" -print); "
        "if [[ -n $air_files ]]; then "
        "mkdir -p HewanAir && mv $air_files HewanAir/; "
        "fi; "
        "if [[ -n $amphibi_files ]]; then "
        "mkdir -p HewanAmphibi && mv $amphibi_files HewanAmphibi/; "
        "fi; "
        "if [[ -n $darat_files ]]; then "
        "mkdir -p HewanDarat && mv $darat_files HewanDarat/; "
        "fi; "
        "if [[ -z $air_files && -z $amphibi_files && -z $darat_files ]]; then "
	"break; "
        "fi; "
        "done'" );
    return a;
}

int bersih(){
int x = system(
	"bash -c '"
        "rm binatang.zip && rmdir list && rm -r HewanAir HewanDarat HewanAmphibi; '" );
return x;
}

int main (){
system("wget --output-document=binatang.zip 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq'");
pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
  if (child_id == 0) {
	char *argv[] = {"unzip", "binatang.zip", "-d", "list", NULL};
	execv("/bin/unzip", argv);
  } 
	else {
        	while ((wait(&status)) > 0);
		pindah();
        		char *folders[] = {"HewanAir", "HewanDarat", "HewanAmphibi", NULL};
			for (int i = 0; folders[i] != NULL; i++) {
			    pid_t zip_child_id = fork();
			    if (zip_child_id == 0) {
				char zip_file[50];
				sprintf(zip_file, "%s.zip", folders[i]);
				char *argv[] = {"zip", "-rq", zip_file, folders[i], NULL};
				execv("/usr/bin/zip", argv);
				exit(EXIT_SUCCESS);
				} 	else if (zip_child_id < 0) {
					exit(EXIT_FAILURE);
				    }
			}
	} while ((wait(&status)) > 0);
	bersih();
	return 0;
}
