#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<time.h>
#include <sys/wait.h>
#include<sys/prctl.h>
#include<stdbool.h> 


void donimg(char *folder){
    pid_t pid;
    int status;
    char nama[60],donl[50];
    // chdir(folder);
    for(int i=0;i<15;i++){
        pid = fork();
        if(pid < 0) exit(EXIT_FAILURE);
        if(pid==0){
            int pxl;
            struct tm *local_time;
            char lokasi[30];
            sprintf(lokasi, "%s",folder);
            time_t current_time = time(NULL);
            pxl = (current_time % 1000) + 50;
            local_time = localtime(&current_time);
            char waktu[30];
            strftime(waktu,30,"%Y-%m-%d_%H:%M:%S.png",local_time);
            // strftime(nama,sizeof(nama),"%s/%Y-%m-%d_%H:%M:%S.png",lokasi,local_time);
            sprintf(nama,"%s/%s",lokasi,waktu);
            sprintf(donl,"https://picsum.photos/%d",pxl);

            execlp("wget","wget","-q","-O",nama,donl,NULL);
        }else {
            waitpid(pid,&status,0);
            sleep(5);
        }
    }
    pid = fork();
    if(pid<0) exit(EXIT_FAILURE);
    if(pid==0){
        char namazip[50];
        sprintf(namazip,"%s.zip",folder);
        execlp("zip","zip","-r",namazip,folder,NULL);
    }else{
        waitpid(pid,&status,0);
        pid = fork();
        if (pid==0) execlp("rm","rm","-r",folder,NULL);
    }

}

void buatfolder(char *nama){
    pid_t tid;
    int status;
    tid = fork();
    if(tid<0) exit(EXIT_FAILURE);
    if(tid==0) execlp("mkdir","mkdir",nama,NULL);
    else{
        waitpid(tid,&status,0);
    }
}

void pembunuh(char *argv[],pid_t pid,pid_t sid){
    FILE *kiler = fopen("killer.c", "w");
    int status;
    char isi[1200];
    if(strcmp(argv[1],"-a")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n\n"
                "#include <sys/wait.h>\n"
                
                "int main() {\n"
                "    int status;\n"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"pkill\",\"pkill\",\"-9\",\"-s\",\"%d\",NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"All processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",sid);
    }
    if(strcmp(argv[1],"-b")==0){
        fprintf(kiler, "#include <stdio.h>\n"
                "#include <stdlib.h>\n"
                "#include <unistd.h>\n"
                "#include <signal.h>\n"
                "#include <sys/wait.h>\n"
                
                "int main() {\n"
                "    int status"
                "    pid_t pid = fork();\n"
                "    if (pid == 0) {\n"
                "        execlp(\"kill\", \"kill\", \"-9\", \"%d\", NULL);\n"
                "    }\n"
                "    else {\n"
                "        waitpid(pid, &status, 0);\n"
                "        printf(\"Processes killed.\\n\");\n"
                "        execlp(\"rm\",\"rm\",\"killer\",NULL);\n"
                "    }\n"
                "    return 0;\n"
                "}\n",getpid());
    }
    fclose(kiler);

    pid = fork();
    if(pid==0){
        execlp("gcc","gcc","killer.c","-o","killer",NULL);
    }else waitpid(pid,&status,0);

    pid = fork();
    if(pid==0){
        execlp("rm","rm","killer.c",NULL);
    }else waitpid(pid,&status,0);

}

int main(int argc, char *argv[]) {

    if (argc < 2) {
     printf("Usage: %s [-a | -b]\n", argv[0]);
     exit(EXIT_FAILURE);
    }
  pid_t pid, sid;// Variabel untuk menyimpan PID
  pid = fork();// Menyimpan PID dari Child Process
  if (pid < 0) exit(EXIT_FAILURE);
  if (pid > 0) exit(EXIT_SUCCESS);
  umask(0);
  sid = setsid();
  if (sid < 0) exit(EXIT_FAILURE);

  pembunuh(argv,pid,sid);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    // Tulis program kalian di sini
    pid_t pid;
    char foldname[30];
    struct tm *local_time;
    time_t current_time = time(NULL);
    local_time = localtime(&current_time);
    strftime(foldname, sizeof(foldname), "%Y-%m-%d_%H:%M:%S", local_time);
    buatfolder(foldname);

    pid = fork();
    if(pid<0)exit(EXIT_FAILURE);
    if(pid==0){
        donimg(foldname);
    }
    sleep(30);
  }
}
