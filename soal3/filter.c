#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int status;
pid_t pid;

void move(char *posisi){
    pid_t ppit;
    int status;

    DIR *dir;
    struct dirent *ent;
    char *dir_path = "players";

    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("Failed to open directory");
    }

    while ((ent = readdir(dir)) != NULL) {// loop untuk membaca setiap file dalam direktori
        if (ent->d_type == DT_REG) { // hanya memeriksa file regular
            char *filename = ent->d_name;
            char *pos = strstr(filename, posisi); // mencari posisi team_name pada nama file

            if (pos != NULL) { // jika ditemukan, move file tersebut
                char *path = malloc(strlen(dir_path) + strlen(filename) + 200); // alokasi memori untuk path
                char *dest_path = malloc(strlen(posisi) + strlen(dir_path) + 200);
                sprintf(path, "%s/%s", dir_path, filename); // gabungkan direktori dan nama file menjadi path lengkap
                sprintf(dest_path, "%s/%s",dir_path ,posisi);
                ppit = fork();
                if(ppit<0) exit(EXIT_FAILURE);
                else if(ppit==0) execlp("mv","mv",path,dest_path,NULL);
                else waitpid(ppit, &status,0);

                free(path);// bebaskan memori yang sudah dialokasi
                free(dest_path);
            }
        }
    }
    closedir(dir);
}

void donlot(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("wget","wget","--no-check-certificate","https://drive.google.com/u/0/uc?id=1uQtvHtZEMJwRIjrF1u_8MlpctucvxurT&export=download","-O","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void uzip(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("unzip","unzip","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void hapus(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("rm","rm","players.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void sortir(){
  DIR *dir;
  struct dirent *ent;
  char *dir_path = "./players";
  dir = opendir("players");
  if(dir != NULL){
    while((ent = readdir(dir))!=NULL){//perulangan cek setiap file
      if(ent->d_type==DT_REG){//hanya file reguler
        if(strstr(ent->d_name, "ManUtd")==NULL){//cek apakah file ada nama ManUtd
        //   char path[500];
          char *path = malloc(strlen(dir_path) + strlen(ent->d_name) + 2);
          sprintf(path, "players/%s", ent->d_name);
          remove(path)==0;
          free(path);
        }
      }
    }
  }
}

void tigac(){
    pid_t pid1,pid2,pid3,pid4;

    pid1=fork();
    if(pid1<0){
        exit(EXIT_FAILURE);
    }else if(pid1==0){
        pid_t cpid1=fork();
        if(cpid1<0) exit(EXIT_FAILURE);
        else if(cpid1==0) execlp("mkdir","mkdir","players/Kiper",NULL);
        else {
            waitpid(cpid1, &status,0);
            move("Kiper");
        }
        exit(0);
    }

    pid2=fork();
    if(pid2<0){
        exit(EXIT_FAILURE);
    }else if(pid2==0){
        pid_t cpid2=fork();
        if(cpid2<0) exit(EXIT_FAILURE);
        else if(cpid2==0) execlp("mkdir","mkdir","players/Bek",NULL);
        else {
            waitpid(cpid2, &status,0);
            move("Bek");
        }
        exit(0);
    }

    pid3=fork();
    if(pid3<0){
        exit(EXIT_FAILURE);
    }else if(pid3==0){
        pid_t cpid3=fork();
        if(cpid3<0) exit(EXIT_FAILURE);
        else if(cpid3==0) execlp("mkdir","mkdir","players/Gelandang",NULL);
        else {
            waitpid(cpid3, &status,0);
            move("Gelandang");
        }
        exit(0);
    }

    pid4=fork();
    if(pid4<0){
        exit(EXIT_FAILURE);
    }else if(pid4==0){
        pid_t cpid4=fork();
        if(cpid4<0) exit(EXIT_FAILURE);
        else if(cpid4==0) execlp("mkdir","mkdir","players/Penyerang",NULL);
        else {
            waitpid(cpid4, &status,0);
            move("Penyerang");
        }
        exit(0);
    }
    waitpid(pid1, &status,0);
    waitpid(pid2, &status,0);
    waitpid(pid3, &status,0);
    waitpid(pid4, &status,0);
}

typedef struct {
  int rating;
  char filename[300];
} Pemain;

int bandingkan_rate(const void* a, const void* b) {
  const Pemain* pemain_a = (const Pemain*)a;
  const Pemain* pemain_b = (const Pemain*)b;
  return pemain_b->rating - pemain_a->rating;
}

void ambil_rate_player(const char* position, Pemain* pemain, int* count) {
  struct dirent* entry; DIR* dir;
  char directory[100];

  sprintf(directory,"players/%s", position);
  dir = opendir(directory);

	if (dir) {
	    for (entry = readdir(dir); entry; entry = readdir(dir)) {
		if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG) {
		    int rating;
		    sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
		    strcpy(pemain[*count].filename, entry->d_name);
		    pemain[*count].rating = rating;
		    (*count)++;
		}
	    }
	    closedir(dir);
	} else {
	    exit(EXIT_FAILURE);
	}
}

void BuatTim(int bek, int gelandang, int penyerang) {
  Pemain  pemain_gelandang[100], pemain_penyerang[100], kiper[1], pemain_bek[100];
  int  jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;
  ambil_rate_player("Gelandang", pemain_gelandang, &jumlah_gelandang);
  ambil_rate_player("Penyerang", pemain_penyerang, &jumlah_penyerang);
  ambil_rate_player("Kiper", kiper, &jumlah_kiper);
  ambil_rate_player("Bek", pemain_bek, &jumlah_bek);

  qsort(pemain_gelandang, jumlah_gelandang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_penyerang, jumlah_penyerang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_bek, jumlah_bek, sizeof(Pemain), bandingkan_rate);

  char filepath[300];
  sprintf(filepath,"Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE* file = fopen(filepath, "w");

  if (file) {
  	int i;
	    fprintf(file, "Posisi Kiper:\n%s\n\n", kiper[0].filename);
	    
	    fprintf(file, "Posisi Bek:\n");
	    for (i = 0; i < bek && i < jumlah_bek; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_bek[i].filename);
	    }
	    
	    fprintf(file, "\n Posisi Gelandang:\n");
	    for (i = 0; i < gelandang && i < jumlah_gelandang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_gelandang[i].filename);
	    }
	    
	    fprintf(file, "\nPosisi Penyerang:\n");
	    for (i = 0; i < penyerang && i < jumlah_penyerang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_penyerang[i].filename);
	    }

	    fclose(file);
	    printf("Lineup saved\n");
  } else {
	    printf("Error\n");
	    exit(EXIT_FAILURE);
  }
}

int main(int argc, char *argv[]) {

    if (argc < 3) {
     printf("Usage: %s [bek] [gelandang] [penyerang]\n", argv[0]);
     exit(EXIT_FAILURE);
    }

    donlot();
    uzip();
    hapus();
    sortir();
    tigac();

    int bek,gelandang,penyerang;
    BuatTim(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));//atoi untuk mengubah argumen argv yang awalnya string menjadi integer
}
